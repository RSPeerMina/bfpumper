import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.component.chatter.ClanChat;
import org.rspeer.runetek.api.component.tab.Tab;
import org.rspeer.runetek.api.component.tab.Tabs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.listeners.SkillListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.event.types.SkillEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ScriptMeta(
        name = "BFPumper",
        desc = "Operates the pump at blast furnace, start downstairs and in a cc ",
        developer = "Mina",
        category = ScriptCategory.OTHER)

public class BFPumper extends Script implements RenderListener, SkillListener {

    Player me = Players.getLocal();
    private int XP_GAINED = 0;
    String ccWorld;
    int world;
    StopWatch timer;
    boolean inCC;
    int x = 0;

    public void onStart() {
        timer = StopWatch.start();
    }

    public int loop() {
        SceneObject pump = SceneObjects.getNearest("Pump");

        if (ClanChat.isInChannel()) {
            inCC = true;
            ccWorld = ClanChat.getChannelName();
        } else {
            inCC = false;
        }

        if (x <= Random.nextInt(120, 240)) {
            x++;
        } else {
            if (!Tabs.isOpen(Tab.SKILLS)) {
                Tabs.open(Tab.SKILLS);
            } else {
                Tabs.open(Tab.INVENTORY);
            }

            x = 0;
        }

        if (inCC) {
            Matcher m = Pattern.compile("\\d+").matcher(ccWorld);
            if(m.find()) {
                world = Integer.parseInt(m.group());
            }

            if (world <= 99) {
                world += 300;
            }

            if (Worlds.getCurrent() != world) {
                Time.sleep(2000, 4000);
                WorldHopper.hopTo(world);
                Time.sleep(2000, 4000);
            } else {
                if (pump != null && !me.isAnimating()) {
                    if (pump.containsAction("Operate")) {
                        Log.info("[DEBUG] Operating");
                        pump.interact("Operate");
                        Time.sleepUntil(me::isAnimating, 60000, 240000);
                    }
                }
            }
        } else {
            if (pump != null && !me.isAnimating()) {
                if (pump.containsAction("Operate")) {
                    Log.info("[DEBUG] Operating");
                    pump.interact("Operate");
                    Time.sleepUntil(me::isAnimating, 60000, 240000);
                }
            }
        }
        return 1000;
    }

    @Override
    public void notify(SkillEvent skillEvent) {
        if(skillEvent.getType() == SkillEvent.TYPE_EXPERIENCE) {
            XP_GAINED += 2;
        }
    }


    @Override
    public void notify(RenderEvent e) {
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 35;
        int x = 10;
        g2.setColor(Color.PINK);
        g2.drawString("BFPumper by Mina", x, y);
        g2.drawString("Runtime: " + timer.toElapsedString(), x, y += 20);
        g2.drawString("XP gained: " + XP_GAINED, x, y += 20);
    }

    public void onStop() {
        Log.fine("Thank you for using BFPumper by Mina!");
        setStopping(true);
    }
}
